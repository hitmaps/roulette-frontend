export default class HitmapsApiDisguise {
    public id!: number;
    public name!: string;
    public image!: string;
    public order!: number;
    public suit!: boolean;

    constructor(init?: Partial<HitmapsApiDisguise>) {
        Object.assign(this, init);
    }

    public toRouletteFormat(): any {
        return {
            name: this.name,
            tileUrl: this.image
        };
    }
}
