import SpinResult from '@/components/SpinResult';
import Condition from '@/components/Condition';
import Complication from "@/components/Complication";

export default class CurrentSpin {
    public mission!: any;
    public targetConditions!: any[];
    public additionalObjectives!: any[];

    constructor(init?: Partial<CurrentSpin>) {
        Object.assign(this, init);
    }

    public get allConditions(): SpinResult[] {
        const formattedTargetConditions = this.targetConditions.map(x => new SpinResult({
            type: 'KILL',
            target: x.target,
            killMethod: new Condition(x.killMethod),
            disguise: new Condition(x.disguise),
            complications: x.complications.map((y: any) => new Complication(y))
        }));
        const formattedAdditionalObjectives = this.additionalObjectives.map(x => new SpinResult({
            type: 'OBJECTIVE',
            target: x.objective,
            killMethod: new Condition(x.completionMethod),
            disguise: new Condition(x.disguise),
            complications: x.complications.map((y: any) => new Complication(y))
        }));

        return [...formattedTargetConditions, ...formattedAdditionalObjectives];
    }
}
