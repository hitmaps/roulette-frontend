import moment from 'moment-timezone';

export default class Utils {
    public static websocketsSupported(): boolean {
        return 'WebSocket' in window || 'MozWebSocket' in window;
    }

    public static getRandomNumber(max: number): number {
        return Math.floor(Math.random() * Math.floor(max));
    }

    public static isLoggedIn(cookies: any): boolean {
        return cookies.isKey('discord-access-token') && cookies.isKey('discord-token-type');
    }

    public static logout(cookies: any): void {
        cookies.remove('discord-access-token');
        cookies.remove('discord-token-type');
    }

    public static copyToClipboard(content: string): Promise<any> {
        return navigator.clipboard.writeText(content);
    }

    public static getDiscordHeader(cookies: any): any {
        if (!cookies.isKey('discord-token-type') || !cookies.isKey('discord-access-token')) {
            return null;
        }

        return {
            headers: {
                'X-Discord-Token': `${cookies.get('discord-token-type')} ${cookies.get('discord-access-token')}`
            }
        };
    }

    public static formatDateTime(date: Date|string, format: string, timezone: string = Intl.DateTimeFormat().resolvedOptions().timeZone): string {
        return moment(date).tz(timezone).format(format);
    }
}
