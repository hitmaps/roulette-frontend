export default class Mission {
    public id!: number;
    public gameOrder!: number;
    public locationOrder!: number;
    public missionOrder!: number;
    public name!: string;
    public gameUri!: string;
    public location!: string;
    public locationName!: string;
    public variant!: string;
    public tileUrl!: string;
    public locationTileUrl!: string;
    public missionType!: string;

    constructor(init?: Partial<Mission>) {
        Object.assign(this, init);
    }
}
