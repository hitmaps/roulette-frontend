export default class Condition {
    public name: string = '';
    public tileUrl: string = '';
    public chosen: boolean = false;
    public selectedVariant?: string;
    public killType: string = '';

    constructor(init?: Partial<Condition>) {
        Object.assign(this, init);
    }
}
