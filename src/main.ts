import {createApp} from "vue";
import App from './App.vue';
import router from './router';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import CountryFlag from 'vue-country-flag-next';
import 'vue-datepicker-next/index.css';
import {VueToastr} from 'vue-toastr';
import 'vue-toastr/dist/style.css';
import 'animate.css';

const app = createApp(App);

app.config.globalProperties.$http = axios;
const port = document.location.port ? `:${document.location.port}` : '';
app.config.globalProperties.$vueDomain = `${document.location.protocol}//${window.location.hostname}${port}`;
app.config.globalProperties.$hitmapsDomain = 'https://api.hitmaps.com';

switch (window.location.hostname) {
    case 'localhost':
        app.config.globalProperties.$apiDomain = 'https://localhost:7141';
        app.config.globalProperties.$tourneyDomain = 'https://localhost:7142';
        app.config.globalProperties.$domain = 'http://localhost:3000';
        break;
    case 'testroulette.hitmaps.com':
        app.config.globalProperties.$apiDomain = 'https://testrouletteapi.hitmaps.com';
        app.config.globalProperties.$tourneyDomain = 'https://testtournamentsapi.hitmaps.com';
        app.config.globalProperties.$domain = `${document.location.protocol}//${window.location.hostname}`;
        break;
    case 'roulette.hitmaps.com':
    default:
        app.config.globalProperties.$apiDomain = 'https://rouletteapi.hitmaps.com';
        app.config.globalProperties.$tourneyDomain = 'https://tournamentsapi.hitmaps.com';
        app.config.globalProperties.$domain = `${document.location.protocol}//${window.location.hostname}`;
        break;
}
app.use(VueToastr, {
    defaultPosition: 'toast-bottom-center',
    defaultTimeout: 5000,
    defaultProgressBar: true,
    defaultClassNames: ["animated", "zoomInUp"]
});
app.component('country-flag', CountryFlag);
app.use(router);

app.mount('#app');
