export default class MapSelection {
    public id!: number;
    public hitmapsSlug!: string;
    public chosenByName?: string;
    public complete!: boolean;
    public winnerName?: string;

    constructor(init?: Partial<MapSelection>) {
        Object.assign(this, init);
    }
}
