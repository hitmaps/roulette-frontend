import Player from '@/components/HitmapsTournaments/Player';
import MapSelection from '@/components/HitmapsTournaments/MapSelection';
import Timer from '@/components/HitmapsTournaments/Timer';

export default class TourneyData {
    public tourneyId!: number;
    public overlayTheme?: string;
    public matchComplete: boolean = false;
    public pointsForVictory?: number;
    public maps: MapSelection[] = [];
    public players: Player[] = [];
    public rouletteId!: string;
    public enabled: boolean = false;
    public spinSendTime?: any;
    public matchDuration?: number = 60;
    public matchTime: Date = new Date();
    public showSpin: boolean = false;
    public showTimer: boolean = false;
    public statusText: string = '';
    public scoreHistory: any[] = [];
    public timer!: Timer;
}
