import { createRouter, createWebHistory } from 'vue-router';

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('../views/Home.vue'),
        },
        {
            path: '/lottie-test',
            name: 'TestLottieFrames',
            component: () => import('../views/TestLottieFrames.vue')
        },
        {
            path: '/slots-test',
            name: 'TestSlots',
            component: () => import('../views/TestSlots.vue')
        },
        {
            path: '/auth',
            name: 'auth',
            component: () => import('../views/DiscordAuth.vue'),
        },
        {
            path: '/tourney-admin/:matchupId',
            name: 'tourney-admin',
            component: () => import('../views/TourneyAdmin.vue'),
            props: true
        },
        {
            path: '/matchup/:matchupId',
            name: 'basic-matchup-client',
            component: () => import('../views/BasicMatchupClient.vue'),
            props: true
        },
        {
            path: '/tourney-participant/:matchupId/:playerId',
            name: 'tourney-participant',
            component: () => import('../views/MatchupClient.vue'),
            props: true
        },
        {
            path: '/overlay/:matchupId',
            name: 'wide-overlay',
            component: () => import('../views/WideOverlay.vue'),
            props: true
        },
        {
            path: '/vertical-overlay/:matchupId',
            name: 'vertical-overlay',
            component: () => import('../views/VerticalOverlay.vue'),
            props: true
        },
        {
            path: '/text-overlay/:matchupId',
            name: 'text-only-overlay',
            component: () => import('../views/TextOnlyOverlay.vue'),
            props: true
        },
        {
            path: '/compact-overlay/:matchupId',
            name: 'compact-overlay',
            component: () => import('../views/CompactOverlay.vue'),
            props: true
        },
        {
            path: '/tournament-overlay/rr15/:matchupId',
            name: 'rr-15-overlay',
            component: () => import('../views/TournamentOverlay/Rr15Overlay.vue'),
            props: true
        },
        {
            path: '/tournament-overlay/:eventSlug/:matchupId',
            name: 'tournament-overlay',
            component: () => import('../views/TournamentOverlay/TournamentOverlay.vue'),
            props: true
        }
    ]
});

export default router;
