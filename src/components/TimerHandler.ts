import * as workerTimers from 'worker-timers';

export default class TimerHandler {
    // tslint:disable-next-line
    public static setTimeout(func: Function, delay: number): number {
        return workerTimers.setTimeout(func, delay);
    }

    public static clearTimeout(timerId: number|null): void {
        if (timerId === null) {
            return;
        }

        try {
            workerTimers.clearTimeout(timerId);
        } catch (e) {
            // Ignored
            return;
        }
    }

    // tslint:disable-next-line
    public static setInterval(func: Function, delay: number): number {
        return workerTimers.setInterval(func, delay);
    }

    public static clearInterval(timerId: number|null): void {
        if (timerId === null) {
            return;
        }

        try {
            workerTimers.clearInterval(timerId);
        } catch (e) {
            // Ignored
            return;
        }
    }
}
