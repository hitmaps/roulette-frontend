import PlayerStatus from '@/components/HitmapsTournaments/PlayerStatus';

export default class Player {
    public publicId!: string;
    public name!: string;
    public avatarUrl?: string;
    public lastPing!: string;
    public completeTime?: string;
    public score!: number;
    public status: PlayerStatus = new PlayerStatus();
    public forfeit!: boolean;

    constructor(init?: Partial<Player>) {
        Object.assign(this, init);
    }
}
