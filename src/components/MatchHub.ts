import {HubConnection, HubConnectionBuilder, HubConnectionState, LogLevel} from '@microsoft/signalr';

export default class MatchHub {
    private connection!: HubConnection;
    private startedPromise!: Promise<void>;


    constructor(baseUrl: string) {
        this.connection = new HubConnectionBuilder()
            .withUrl(`${baseUrl}/ws/match-hub`)
            .withAutomaticReconnect()
            .configureLogging(LogLevel.Information)
            .build();
    }

    public start(): Promise<void> {
        const start = (): Promise<void> => {
            this.startedPromise = this.connection.start().catch(err => {
                console.error('Failed to connect to hub', err);
                return new Promise((res, rej) => {
                    setTimeout(() => start().then(res).catch(rej), 5000);
                });
            });
            return this.startedPromise;
        };
        this.connection.onclose(() => start());

        // noinspection JSIgnoredPromiseFromCall
        return start();
    }

    public stop(): Promise<void> {
        return this.connection.stop();
    }

    public state(): HubConnectionState {
        return this.connection.state;
    }

    public onReconnecting(callback: (error: Error|undefined) => void): void {
        this.connection.onreconnecting(callback);
    }

    public onReconnected(callback: (connectionId: string|undefined) => void): void {
        this.connection.onreconnected(callback);
    }

    public onClose(callback: (error: Error|undefined) => void): void {
        this.connection.onclose(callback);
    }

    public on(event: string, callback: (args: any[]) => void): void {
        this.connection.on(event, callback);
    }

    public off(event: string): void {
        this.connection.off(event);
    }

    public joinMatch(matchId: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('JoinMatchGroup', matchId))
            .catch(console.error);
    }

    public adminJoinMatch(matchId: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminJoinMatchGroup', matchId))
            .catch(console.error);
    }

    public leaveMatch(matchId: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('LeaveMatchGroup', matchId))
            .catch(console.error);
    }

    public adminLeaveMatch(matchId: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminLeaveMatchGroup', matchId))
            .catch(console.error);
    }

    public adminJoinMessageAck(matchId: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminJoinMessageAck', matchId))
            .catch(console.error);
    }

    public adminLeaveMessageAck(matchId: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminJoinMessageAck', matchId))
            .catch(console.error);
    }

    public participantJoinMessage(matchId: string, playerId: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('ParticipantJoinMessageGroup', matchId, playerId))
            .catch(console.error);
    }

    public participantLeaveMessage(matchId: string, playerId: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('ParticipantLeaveMessageGroup', matchId, playerId))
            .catch(console.error);
    }

    public sendHeartbeat(matchId: string, playerPublicId: string, timerFingerprint: string): Promise<any> {
        return this.startedPromise
            .then(() => this.connection.invoke<any>('SendHeartbeat', matchId, playerPublicId, timerFingerprint))
            .catch(console.error);
    }
}
